(defproject codamic/medusa "0.1.0-SNAPSHOT"
  :description "Distributed and encrypted key value storage service."
  :url "http://example.com/FIXME"
  :license {:name "GPLv2"
            :url "https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html"}
  :dependencies [[org.clojure/clojure     "1.9.0"]
                 [codamic/hellhound.core  "1.0.0-SNAPSHOT"]
                 [codamic/hellhound.http  "1.0.0-SNAPSHOT"]
                 [codamic/hellhound.utils "1.0.0-SNAPSHOT"]
                 [com.walmartlabs/lacinia "0.30.0"]]


  :aliases {"fig" ["trampoline" "run" "-m" "figwheel.main"]
            "build-dev" ["trampoline" "run" "-m" "figwheel.main" "-b" "dev" "-r"]}

  :resource-paths ["resources"]
  :source-paths ["src/clj" "src/cljs"]
  :main ^:skip-aot medusa.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
