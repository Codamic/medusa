(ns user
  (:require
   [medusa.system :as system]
   [hellhound.system :as sys]
   [clojure.tools.namespace.repl :as repl]))


(def system-store nil)

(defn init
  "Constructs the current development system."
  []
  (alter-var-root #'system-store
    (constantly (system/dev))))

(defn start
  "Starts the current development system."
  []
  (alter-var-root #'system-store #(sys/start %)))

(defn stop
  "Shuts down and destroys the current development system."
  []
  (alter-var-root #'system-store #(sys/stop %)))

(defn go
  "Initializes the current development system and starts it running."
  []
  (init)
  (start))

(defn reset []
  (stop)
  (repl/refresh :after 'user/go))

(comment
  (go)
  (reset)
  (stop))
