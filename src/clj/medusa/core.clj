(ns medusa.core
  (:gen-class)
  (:require
   [medusa.system :as system]
   ;;[hellhound.repl :as repl]
   [hellhound.system :as sys]
   [clojure.tools.namespace.repl :as repl]
   [hellhound.system.development :as d]))

(d/setup-development medusa.core  'system/dev)

(defn -main
  [& args]
  (sys/set-system! system/dev)
  ;;(repl/start!)
  (sys/start!))
  ;;(d/start! #'medusa.system/dev))

;; (def system-store nil)

;; (defn init
;;   "Constructs the current development system."
;;   []
;;   (alter-var-root #'system-store
;;     (constantly (system/dev))))

;; (defn start
;;   "Starts the current development system."
;;   []
;;   (alter-var-root #'system-store #(sys/start %)))

;; (defn stop
;;   "Shuts down and destroys the current development system."
;;   []
;;   (alter-var-root #'system-store #(sys/stop %)))

;; (defn go
;;   "Initializes the current development system and starts it running."
;;   []
;;   (init)
;;   (start))

;; (defn reset []
;;   (stop)
;;   (repl/refresh :after 'medusa.core/go))


(comment
  (start1)
  (reset1)
  (stop1))



(comment
  (start)
  (restart)
  (stop)
  (hellhound.system.core/restart-component! (sys/system) :hellhound.components.webserver/webserver)
  (sys/get-component :hellhound.components.webserver/webserver)
  ;(repl/stop!)
  (sys/stop!))
