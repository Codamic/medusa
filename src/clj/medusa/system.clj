(ns medusa.system
  (:require
   [hellhound.system :as sys]
   [hellhound.component :refer [deftransform]]
   [hellhound.components.webserver :as webserver]
   [hellhound.components.pretty-printer :as printer]
   [medusa.http.router :as router]
   [medusa.graphql.component :as graphql]
   [medusa.http.handlers :as handlers]))

(defn is-type?
  [v t]
  (= (:type v) t))

(defn graphql?
  [v]
  (is-type? v :graphql))

(defn home-page?
  [v]
  (is-type? v :home-page))

(defn not-found?
  [v]
  (is-type? v :not-found))

(defn dev
  []
  {:components [(webserver/factory)
                router/router
                printer/printer
                graphql/server
                handlers/not-found
                handlers/home-page]

   :workflow
   [;; [::webserver/webserver ::printer/printer]
    ;; [::webserver/webserver ::router/router]

    ;; [::router/router home-page? ::handlers/home-page]
    ;; [::router/router graphql? ::graphql/server]
    ;; [::router/router not-found? ::handlers/not-found]

    ;; [::graphql/server ::webserver/webserver]
    ;; [::handlers/home-page  ::webserver/webserver]
    ;; [::handlers/not-found  ::webserver/webserver]

    {:hellhound.workflow/source ::webserver/webserver
     :hellhound.workflow/sink ::printer/printer}

    {:hellhound.workflow/source ::webserver/webserver
     :hellhound.workflow/sink ::router/router}

    {:hellhound.workflow/source ::router/router
     :hellhound.workflow/predicate home-page?
     :hellhound.workflow/sink ::handlers/home-page}

    {:hellhound.workflow/source ::router/router
     :hellhound.workflow/predicate graphql?
     :hellhound.workflow/sink ::graphql/server}

    {:hellhound.workflow/source ::router/router
     :hellhound.workflow/predicate not-found?
     :hellhound.workflow/sink ::handlers/not-found}

    {:hellhound.workflow/source ::graphql/server
     :hellhound.workflow/sink ::webserver/webserver}

    {:hellhound.workflow/source ::handlers/home-page
     :hellhound.workflow/sink ::webserver/webserver}

    {:hellhound.workflow/source ::handlers/not-found
     :hellhound.workflow/sink ::webserver/webserver}]




   :execution {:mode :multi-thread}
   :logger {:level :info}
   :config
   {::webserver/webserver {:host "localhost"
                           :port 3000
                           :scheme "http"}
    ::graphql/server {:schema "graphql/medusa.edn"
                      :resolvers graphql/resolvers}
    ::router/router {:routes {"/q" :graphql
                              "/"  :home-page}}}})
