(ns medusa.graphql.component
  (:require
   [hellhound.streams        :as streams]
   [hellhound.component      :as com]
   [medusa.graphql.core      :as core]
   [medusa.graphql.resolvers :as resolver]))



(def resolvers
  {:node/all-keys resolver/all-keys})



(defn start!
  [this context]
  (let [schema    (core/compile (:config context))
        executor  (partial core/execute schema)]
    (assoc this
           :schema   schema
           :executor executor)))


(defn stop!
  [this]
  (dissoc this :schema :executor))


(defn consume
  [component f]
  (streams/consume
   (fn [v]
     (let [q (slurp (:body (:request v)))
           processed-v (f component q)]
       (when processed-v
         (println "++++++++++++++++++++")
         (println processed-v)
         (streams/>> (com/output component)
                     (assoc v
                            :response {:status 200
                                       :headers []
                                       :body (str processed-v)})))))
   (com/input component)))


(defn transformer
  [component]
  (let [executor (:executor component)]
    (consume component #(executor %2 %1))))


(def server  {::com/name ::server
              ::com/depends-on []
              ::com/start-fn start!
              ::com/stop-fn stop!
              ::com/fn transformer})
