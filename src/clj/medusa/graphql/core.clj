(ns medusa.graphql.core
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [com.walmartlabs.lacinia.schema :as schema]
   [com.walmartlabs.lacinia :as lacinia]
   [com.walmartlabs.lacinia.expound]
   [com.walmartlabs.lacinia.util :as util]))


(defn compile
  [{:keys [schema resolvers] :as config}]
  (println "<<<<<<<<<<<<<<<")
  (println schema)
  (-> (io/resource schema)
      slurp
      edn/read-string
      (util/attach-resolvers resolvers)
      schema/compile))


(defn execute
  [schema query context]
  (lacinia/execute schema query nil context))
