(ns medusa.http.handlers
  (:require
   [clojure.java.io :as io]
   [hellhound.http.ring.template :as template]
   [hellhound.component :refer [deftransform]]))


;; (defn home-page
;;   [context]
;;   (template/render "index.html" {}))
;; (httputils/html-response "public/index.html"))

(deftransform home-page
  [this v]
  (println "---------------------")
  (clojure.pprint/pprint v)
  (println "---------------------")
  (assoc v :response (template/render "public/index.html" {})))

(deftransform not-found
  [this v]
  (assoc v :response {:status 404 :body "NOT FOUND!"}))
