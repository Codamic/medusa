(ns medusa.http.router
  (:require
   [hellhound.component :refer [deftransform]]))



(deftransform router
  [component v]
  (let [config (:config (:context component))
        routes (:routes config)]
    ;; TODO: validate the routes using spec.
    (if (contains? routes (:uri (:request v)))
      (assoc v :type (get routes (:uri (:request v))))
      (assoc v :type :not-found))))
